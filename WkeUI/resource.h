//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 WkeUI.rc 使用
//
#define INDEX_STYLE                     4
#define IDS_STRING103                   103
#define INDEX_SCRIPT                    103
#define INDEX_HTML                      104
#define JQUERY_SCRIPT                   108
#define FONT_AWESOME                    109
#define GLYPHICON_HTML                  110
#define IVIEW_SCRIPT                    111
#define IVIEW_STYLE                     112
#define VUE_SCRIPT                      113

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        114
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
