#include "wkeui.h"


jsValue DemoProgram::onScriptClose(jsExecState es, jsValue object, jsValue* args, int argCount)
{
	PostQuitMessage(0);
	return jsValue();
}

jsValue DemoProgram::onScriptMinimize(jsExecState es, jsValue object, jsValue* args, int argCount)
{
	SendMessage(hWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
	wkeSetTransparent(UI, false);
	return jsValue();
}

jsValue DemoProgram::onScriptSetWinWidth(jsExecState es, jsValue object, jsValue* args, int argCount)
{
	SendMessage(hWnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
	return jsValue();
}

jsValue DemoProgram::onScriptResizeWindow(jsExecState es, jsValue object, jsValue* args, int argCount)
{
	if (argCount != 2)
		return jsBoolean(false);
	int width = jsToInt(es, args[0]);
	int height = jsToInt(es, args[1]);
	wkeResizeWindow(UI, width, height);
	return jsBoolean(true);
}

jsValue DemoProgram::onScriptMoveToCenter(jsExecState es, jsValue object, jsValue* args, int argCount)
{
	wkeMoveToCenter(UI);
	return jsBoolean(true);
}