
#include "uiconfig.h"
#include "wkeui.h"
#include <iostream>
#include "string.h"
using std::string;
using std::wstring;

WkeUI *NowUI = {0};

char* replaceChar(char *text, char *replace, char *content,int *size = 0)
{
	char *result;
	string stext(text);
	string sreplace(replace);
	string scontent(content);
	int pos = 0;

	while ((pos = stext.find(sreplace)) >= 0)
	{
		stext = stext.replace(pos, sreplace.length(), scontent);
	}

	result = new char[stext.length()+1];
	result[stext.length()] = '\0';
	memcpy(result, stext.c_str(), stext.length());
	*size = stext.length();
	return result;
}
wchar_t* replaceChar(wchar_t *text, wchar_t *replace, wchar_t *content, int *size = 0)
{
	wchar_t *result;
	wstring stext(text);
	wstring sreplace(replace);
	wstring scontent(content);
	int pos = 0;

	while ((pos = stext.find(sreplace)) >= 0)
	{
		stext = stext.replace(pos, sreplace.length(), scontent);
	}

	result = new wchar_t[stext.length() + 1];
	result[stext.length()] = '\0';
	wcscpy_s(result, stext.length(), stext.c_str());
	*size = stext.length();
	return result;
}


//onScriptSetCaptionPlace
INIT_BIND_FUNC(onScriptClose)					//初始化被绑定的成员函数
INIT_BIND_FUNC(onScriptMinimize)
INIT_BIND_FUNC(onScriptResizeWindow)
INIT_BIND_FUNC(onScriptMoveToCenter)
INIT_BIND_FUNC(onScriptSetCaptionPlace)

BIND_FUNCTION_BEGIN								//开始绑定函数
BIND_FUNCTION(onScriptClose, "Close")			//绑定类成员onScriptClose到window.Close
BIND_FUNCTION(onScriptMinimize, "Minimize")
BIND_FUNCTION(onScriptResizeWindow, "reSize")
BIND_FUNCTION(onScriptMoveToCenter, "moveToCenter")
BIND_FUNCTION(onScriptSetCaptionPlace, "setCaptionPlace")

BIND_FUNCTION_END								//结束绑定函数

void fun_onConsoleCallback(wkeWebView webView, void* param, wkeConsoleLevel level, const wkeString message, const wkeString sourceName, unsigned sourceLine, const wkeString stackTrace)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	UI->onConsoleCallback(webView, param, level, message, sourceName, sourceLine, stackTrace);
	return ;
}

bool fun_onWindowClosing(wkeWebView webView, void* param)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	UI->onWindowClosing(webView, param);
	return true;
}

void fun_onWindowDestory(wkeWebView webView, void* param)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	UI->onWindowDestory(webView, param);
}

void fun_onDocumentReady(wkeWebView webView, void* param)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	UI->onDocumentReady(webView, param);
}

void fun_onTitleChange(wkeWebView webView, void* param, const wkeString title)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	UI->onTitleChange(webView, param, title);
}

wkeWebView fun_onCreateView(wkeWebView webView, void* param, wkeNavigationType navType, const wkeString url, const wkeWindowFeatures* features)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	return UI->onCreateView(webView, param, navType, url, features);
}

bool fun_onLoadUrlBegin(wkeWebView webView, void* param, const char *url, void *job)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	return UI->onLoadUrlBegin(webView, param, url, job);
}

void fun_onLoadUrlEnd(wkeWebView webView, void* param, const char *url, void *job, void* buf, int len)
{
	USE_UI_CLASS *UI = (USE_UI_CLASS*)param;
	return UI->onLoadUrlEnd(webView, param, url, job, buf, len);
}

/*
*载入HTML创建UI
*/
WkeUI::WkeUI(wchar_t* HTML, int HtmlSize)
{
	int cy = GetSystemMetrics(SM_CYSCREEN);
	int cx = GetSystemMetrics(SM_CXSCREEN);
	InitUI();
	CreateUI(WKE_WINDOW_TYPE_TRANSPARENT, NULL, 0, 0, cx, cy);
	ShowUI();
	LoadHtml(HTML, HtmlSize == -1 ? wcslen(HTML): HtmlSize);
}

WkeUI::WkeUI(utf8* HTML, int HtmlSize)
{
	int cy = GetSystemMetrics(SM_CYSCREEN);
	int cx = GetSystemMetrics(SM_CXSCREEN);
	InitUI();
	InitUI();
	CreateUI(WKE_WINDOW_TYPE_TRANSPARENT, NULL, 0, 0, cx, cy);
	ShowUI();
	LoadHtml(HTML, HtmlSize == -1 ? strlen(HTML) : HtmlSize);
}

WkeUI::WkeUI()
{
	int cy = GetSystemMetrics(SM_CYSCREEN);
	int cx = GetSystemMetrics(SM_CXSCREEN);
	InitUI();
	CreateUI(WKE_WINDOW_TYPE_TRANSPARENT, NULL, 0, 0, cx, cy);
	ShowUI();
	LoadHtml();
}
WkeUI::~WkeUI()
{
	ReleaseUI();
}

void WkeUI::InitUI()
{
	wkeInitialize();
	wkeProxy* _proxy = new wkeProxy;
	_proxy->type = wkeProxyType::WKE_PROXY_NONE;
	wkeSetProxy(_proxy);
}

LRESULT CALLBACK HookWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return NowUI->OnMessage(hWnd,uMsg,wParam,lParam);
}

wkeWebView WkeUI::CreateUI(wkeWindowType type, HWND parent, int x, int y, int width, int height)
{
	wkeWebView result = wkeCreateWebWindow(type, parent, x, y, width, height);
	wkeSetCspCheckEnable(result, false);
	this->UI = result;
	NowUI = this;
	InsertUIPointer();
	BindFunction();
	BindEvents();
	LoadAllTemplate();
	hWnd = wkeGetWindowHandle(result);
	//m_hHook = SetWindowsHookEx(WH_CALLWNDPROC, &SetHook, NULL, GetCurrentThreadId());
	oldProc = (WNDPROC)SetWindowLong(hWnd, GWL_WNDPROC, (LONG)HookWndProc);//GWL_WNDPROC 这个需要切换成x86编译模式
	SetWindowLong(hWnd, GWL_STYLE, WS_SYSMENU);
	return result;
}
void WkeUI::ShowUI()
{
	wkeShowWindow(this->UI,true);
}

void WkeUI::MissUI()
{
	wkeShowWindow(this->UI, false);
}

void WkeUI::ReleaseUI()
{
	wkeFinalize();
}

void WkeUI::BindEvents()
{
	wkeOnConsole(this->UI, &fun_onConsoleCallback, this);
	wkeOnWindowClosing(this->UI, &fun_onWindowClosing, this);
	wkeOnWindowDestroy(this->UI, &fun_onWindowDestory, this);
	wkeOnDocumentReady(this->UI, &fun_onDocumentReady, this);
	wkeOnTitleChanged(this->UI, &fun_onTitleChange, this);
	wkeOnCreateView(this->UI, &fun_onCreateView, this);
	wkeOnLoadUrlBegin(this->UI, &fun_onLoadUrlBegin, this);
	wkeOnLoadUrlEnd(this->UI, &fun_onLoadUrlEnd, this);
}

void WkeUI::LoadAllTemplate()
{
	BIND_TEMPLATE(INDEX_SCRIPT,"index_script");
	BIND_TEMPLATE(INDEX_STYLE, "index_style");
	BIND_TEMPLATE(JQUERY_SCRIPT, "jquery_script");//FONT_AWESOME
	BIND_TEMPLATE(VUE_SCRIPT, "vue_script");//FONT_AWESOME
	BIND_TEMPLATE(IVIEW_SCRIPT, "iview_script");
	BIND_TEMPLATE(IVIEW_STYLE, "iview_style");
	BIND_TEMPLATE(FONT_AWESOME, "font-awesome");//GLYPHICON_HTML
	BIND_TEMPLATE(GLYPHICON_HTML, "glyphicon_style");
}
void WkeUI::InsertUIPointer()
{
	execState = wkeGlobalExec(UI);
	window = jsGlobalObject(execState);
	jsValue _this = jsInt((int)this);
	jsSet(execState, window, "_classPointer", _this);
}
void WkeUI::LoadHtml(wchar_t* HTML,int HtmlSize)
{
	HTMLTextW = HTML;
	wkeLoadHTMLW(UI, MergeTemplateHTMLW(HTML));
}

void WkeUI::LoadHtml(utf8* HTML, int HtmlSize)
{
	HTMLText = HTML;
	wkeLoadHTML(UI, MergeTemplateHTML(HTML));
}

void WkeUI::LoadHtml()
{
	char *tData = {0};
	int tDataLen = GetHTMLContent(&tData);
	char *ntData = new char[tDataLen+1];
	memcpy(ntData, tData, tDataLen);
	ntData[tDataLen] = '\0';
	HTMLText = MergeTemplateHTML(ntData);
	wkeLoadHTML(UI, HTMLText);
}

int WkeUI::GetHTMLContentW(wchar_t **srcData, int SrcId)
{
	HMODULE hInst = GetModuleHandle(NULL);
	LPCWSTR IndexSourceName = MAKEINTRESOURCEW(SrcId>0 ? SrcId : INDEX_HTML);
	LPCWSTR SourceType = MAKEINTRESOURCEW(RT_HTML);
	HRSRC hSrc = FindResourceW(hInst, IndexSourceName, MAKEINTRESOURCEW(RT_HTML));
	HGLOBAL hG = LoadResource(hInst,hSrc);
	DWORD Size = SizeofResource(hInst, hSrc);
	char*hData = (char*)LockResource(hG);
	int hDataLen = strlen(hData);
	wchar_t *hwData = new wchar_t[hDataLen];
	ZeroMemory(hwData, hDataLen*2);
	MultiByteToWideChar(CP_UTF8, NULL, hData, -1, hwData, hDataLen);
	*srcData = hwData;
	return Size;
}

int WkeUI::GetHTMLContent(char **srcData, int SrcId)
{
	HMODULE hInst = GetModuleHandle(NULL);
	LPCWSTR IndexSourceName = MAKEINTRESOURCEW(SrcId>0 ? SrcId : INDEX_HTML);
	LPCWSTR SourceType = MAKEINTRESOURCEW(RT_HTML);
	HRSRC hSrc = FindResourceW(hInst, IndexSourceName, MAKEINTRESOURCEW(RT_HTML));
	HGLOBAL hG = LoadResource(hInst, hSrc);
	DWORD Size = SizeofResource(hInst, hSrc);
	*srcData = (char *)LockResource(hG);
	//char *result = new char[Size + 1];
	//ZeroMemory(result, Size + 1);
	//memcpy(result, srcData, Size);
	return Size;
}

bool WkeUI::AppendTemplateFromRes(int SrcId,const char* Name)
{
	char *tData = {0};
	int tDataSize = GetHTMLContent(&tData,SrcId);
	if (MaxTemplateCount <= TemplateCount)
		return false;
	Templates[TemplateCount].name = Name;
	Templates[TemplateCount].data = tData;
	TemplateCount++;
	return true;
}

char* WkeUI::MergeTemplateHTML(char* HTML)
{
	char *Result_first = HTML;
	char *Result_last = Result_first;
	const char *TBefore = "{$";
	const char *TAfter = "}";
	char *templateName = new char[MaxTemplateCount];
	int TBNameLen = 0;
	int strpos = 0;
	int result_first_length = 0;
	int html_size = 0;

	ZeroMemory(templateName, MaxTemplateCount);

	for (int i = 0; i < TemplateCount; i++)
	{
		TBNameLen = strnlen(Templates[i].name, 50);
		if ((TBNameLen + 3) >= MaxTemplateCount)
		{
			OutputDebugString("模版变量名称过长，最高不能超过50个字符！");
			continue;
		}
		templateName = strncat(templateName, TBefore,2);
		templateName = strncat(templateName, Templates[i].name, TBNameLen);
		templateName = strncat(templateName, TAfter, 1);

		//插入模版变量数据
		Result_first = replaceChar(Result_first, templateName, Templates[i].data, &html_size);

		// 内存清理
		result_first_length = html_size;
		if (Result_last)delete[] Result_last;
		Result_last = Result_first;

		ZeroMemory(templateName, MaxTemplateCount);
	}
	delete[] templateName;
	Result_last = new char[html_size + 1];
	memcpy(Result_last, Result_first, html_size);
	Result_last[html_size] = '\0';

	FILE* file = fopen("using_html.htm", "wb+");
	fwrite(Result_last, 1, html_size + 1, file);
	fclose(file);
	return Result_last;
}

wchar_t* WkeUI::MergeTemplateHTMLW(wchar_t* HTML)
{
	wchar_t *Result_first = HTML;
	wchar_t *Result_last = Result_first;
	const wchar_t *TBefore = L"{$";
	const wchar_t *TAfter = L"}";
	wchar_t *templateName = new wchar_t[MaxTemplateCount];
	int TBNameLen = 0;
	int strpos = 0;
	int result_first_length = 0;
	int html_size = 0;

	ZeroMemory(templateName, MaxTemplateCount);

	for (int i = 0; i < TemplateCountW; i++)
	{
		TBNameLen = wcsnlen(TemplatesW[i].name, 50);
		if ((TBNameLen + 3) >= MaxTemplateCount)
		{
			OutputDebugString("模版变量名称过长，最高不能超过50个字符！");
			
		}

		templateName = wcsncat(templateName, TBefore, wcsnlen(TBefore, 5));
		templateName = wcsncat(templateName, TemplatesW[i].name, wcsnlen(TemplatesW[i].name, 50));
		templateName = wcsncat(templateName, TAfter, wcsnlen(TAfter, 5));

		//插入模版变量数据
		Result_first = replaceChar(Result_first, templateName, TemplatesW[i].data, &html_size);

		// 内存清理
		result_first_length = html_size;
		if (Result_last)delete[] Result_last;
		Result_last = Result_first;

		ZeroMemory(templateName, MaxTemplateCount);
	}

	delete[] templateName;
	Result_last = new wchar_t[html_size + 1];
	wcscpy_s(Result_last, wcslen(Result_last), Result_first);
	Result_last[html_size] = '\0';

	return Result_last;
}
char* WkeUI::MergeTemplateHTML(int hId)
{

	return 0;
}

LRESULT WkeUI::OnMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT rc;
	//修改系统事件
	if (uMsg == WM_SYSCOMMAND)
	{
		switch (wParam)
		{
			case SC_RESTORE:
			{
				wkeSetTransparent(UI, true);
			}
		}
	}
	//调用原窗口回调
	rc = CallWindowProc(oldProc, hWnd, uMsg, wParam, lParam);
	//鼠标点击区域事件
	if (uMsg == WM_NCHITTEST)
	{
		int pointX = LOWORD(lParam);
		int pointY = HIWORD(lParam);
		RECT hWndRect;
		GetWindowRect(hWnd, &hWndRect);
		if (pointX<(hWndRect.left + NowUI->captionPlace.right) && pointX>(hWndRect.left + NowUI->captionPlace.left))
		{
			if (pointY<(hWndRect.top + NowUI->captionPlace.bottom) && pointY>(hWndRect.top + NowUI->captionPlace.top))
			{
				rc = HTCAPTION;
				//OutputDebugString("caption\n");
				CallWindowProc(oldProc, hWnd, WM_MOUSEMOVE, wParam, lParam);
			}
			else {
				rc = HTCLIENT;
				//OutputDebugString("client\n");
			}
		}
		else {
			rc = HTCLIENT;
			//OutputDebugString("client\n");
		}
	}
	//鼠标移出窗口事件
	if (uMsg == WM_MOUSELEAVE)
	{
		CallWindowProc(oldProc, hWnd, WM_MOUSEMOVE, wParam, lParam);
		MouseTracking = false;
	}
	//鼠标移动事件
	if (uMsg == WM_MOUSEMOVE && MouseTracking == false)
	{
		TRACKMOUSEEVENT csTME;
		csTME.cbSize = sizeof(csTME);
		csTME.dwFlags = TME_LEAVE;
		csTME.hwndTrack = hWnd;
		csTME.dwHoverTime = 1;
		TrackMouseEvent(&csTME);
		MouseTracking = true;
	}
	//if (uMsg == WM_NCDESTROY)
	//	UnhookWindowsHookEx(m_hHook);
	return rc;
}

void WkeUI::onConsoleCallback(wkeWebView webView, void* param, wkeConsoleLevel level, const wkeString message, const wkeString sourceName, unsigned sourceLine, const wkeString stackTrace)
{
	const utf8* _message = wkeToString(message);
	const utf8* _sourceName = wkeToString(sourceName);
	const utf8* _stackTrace = wkeToString(stackTrace);
	return ;
}

bool WkeUI::onWindowClosing(wkeWebView webView, void* param)
{
	PostQuitMessage(0);
	return true;
}

void WkeUI::onWindowDestory(wkeWebView webView, void* param)
{

}

void WkeUI::onDocumentReady(wkeWebView webView, void* param)
{

}

void WkeUI::onTitleChange(wkeWebView webView, void* param, const wkeString title)
{

}

wkeWebView WkeUI::onCreateView(wkeWebView webView, void* param, wkeNavigationType navType, const wkeString url, const wkeWindowFeatures* features)
{
	return webView;
}

bool WkeUI::onLoadUrlBegin(wkeWebView webView, void* param, const char *url, void *job)
{
	return false;
}

void WkeUI::onLoadUrlEnd(wkeWebView webView, void* param, const char *url, void *job, void* buf, int len)
{
	//jsData *data = new jsData{ 0 };
	return;
}


jsValue WkeUI::onScriptSetCaptionPlace(jsExecState es, jsValue object, jsValue* args, int argCount)
{
	if (argCount != 4)
		return jsBoolean(false);
	captionPlace.left = jsToInt(es, args[0]);
	captionPlace.top = jsToInt(es, args[1]);
	captionPlace.right = jsToInt(es, args[2]);
	captionPlace.bottom = jsToInt(es, args[3]);
	return jsBoolean(true);//ssssss
}



