
#include "uiconfig.h"
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "wkeui.h"

void MessageLoop()
{
	MSG msg = { 0 };
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

int WINAPI WinMain(
	__in HINSTANCE hInstance,
	__in_opt HINSTANCE hPrevInstance,
	__in LPSTR lpCmdLine,
	__in int nShowCmd)
{
	USE_UI_CLASS *wkeui = new USE_UI_CLASS();
	MessageLoop();
}