#pragma once
#include "uiconfig.h"
#include "wWinMain.h"

typedef struct TemplateW {
	const wchar_t *name;
	wchar_t *data;
}*LPTemplateW;

typedef struct Template {
	const char *name;
	char *data;
}*LPTemplate;

#define BIND_TEMPLATE(a,b)	AppendTemplateFromRes(a,b)
#define BIND_TEMPLATE_W(a,b)	AppendTemplateFromSrcW(a,b)

#define BIND_FUNCTION_BEGIN	\
void WkeUI::BindFunction()\
{	\
jsValue v;

#define BIND_FUNCTION_END	}

#define INIT_BIND_FUNC(func_pointer) \
jsValue func_pointer(jsExecState es, jsValue object, jsValue* args, int argCount)	\
{	\
	int classPointer = jsToInt(es, jsGetGlobal(es,  "_classPointer"));	\
	USE_UI_CLASS *UI = (USE_UI_CLASS *)classPointer;	\
	return UI->func_pointer(es, object, args, argCount);	\
}
#define BIND_FUNCTION(func_pointer,func_name)	\
	jsData *func_pointer##_data = new jsData{ 0 };	\
	func_pointer##_data->callAsFunction = ::func_pointer;	\
	v = jsFunction(execState, func_pointer##_data);	\
	jsSet(execState, window, func_name, v);

#define bind_func											//该函数是用于绑定到全局window的，由javascript调用

#define MaxTemplateCount 50									//模版变量名最大长度

class WkeUI {
public:
	wkeWebView UI;
	wchar_t* HTMLTextW;
	char* HTMLText;
	Template Templates[MaxTemplateCount];
	int TemplateCount = 0;
	TemplateW TemplatesW[MaxTemplateCount];
	int TemplateCountW = 0;
	jsValue window = 0;
	HWND hWnd = 0;
	jsExecState execState = 0;
	bool lockMove = false;
	RECT captionPlace = { 0,0,0,0 };
	HHOOK m_hHook;
	WNDPROC oldProc;
	bool MouseTracking = false;

	WkeUI(wchar_t* HTML, int HtmlSize = -1);
	WkeUI(utf8* HTML, int HtmlSize = -1);
	WkeUI();
	~WkeUI();
	void InitUI();
	wkeWebView CreateUI(wkeWindowType type, HWND parent, int x, int y, int width, int height);
	void ReleaseUI();
	void LoadHtml(wchar_t* HTML, int HtmlSize);
	void LoadHtml(utf8* HTML, int HtmlSize);
	void LoadHtml();
	void ShowUI();
	void MissUI();
	void BindEvents();
	void BindFunction();
	void LoadAllTemplate();
	void InsertUIPointer();
	LRESULT OnMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	int GetHTMLContentW(wchar_t **srcData, int SrcId = 0);
	int GetHTMLContent(char **srcData, int SrcId = 0);
	wchar_t* MergeTemplateHTMLW(wchar_t* HTML);
	char* MergeTemplateHTML(char* HTML);
	char* MergeTemplateHTML(int hId);
	bool AppendTemplateFromRes(int SrcId, const  char* Name);
	bool AppendTemplateFromSrcW(int SrcId, const  wchar_t* Name);

	void onConsoleCallback(wkeWebView webView, void* param, wkeConsoleLevel level, const wkeString message, const wkeString sourceName, unsigned sourceLine, const wkeString stackTrace);
	bool onWindowClosing(wkeWebView webView, void* param);
	void onWindowDestory(wkeWebView webView, void* param);
	void onDocumentReady(wkeWebView webView, void* param);
	void onTitleChange(wkeWebView webView, void* param, const wkeString title);
	wkeWebView onCreateView(wkeWebView webView, void* param, wkeNavigationType navType, const wkeString url, const wkeWindowFeatures* features);
	bool onLoadUrlBegin(wkeWebView webView, void* param, const char *url, void *job);
	void  onLoadUrlEnd(wkeWebView webView, void* param, const char *url, void *job, void* buf, int len);

	bind_func jsValue onScriptSetCaptionPlace(jsExecState es, jsValue object, jsValue* args, int argCount);

	void BindWindowFunction();
	jsValue _cdecl systemCall();
};

class DemoProgram :public WkeUI {
public:
	bind_func jsValue onScriptClose(jsExecState es, jsValue object, jsValue* args, int argCount);
	bind_func jsValue onScriptMinimize(jsExecState es, jsValue object, jsValue* args, int argCount);
	bind_func jsValue onScriptSetWinWidth(jsExecState es, jsValue object, jsValue* args, int argCount);
	bind_func jsValue onScriptResizeWindow(jsExecState es, jsValue object, jsValue* args, int argCount);
	bind_func jsValue onScriptMoveToCenter(jsExecState es, jsValue object, jsValue* args, int argCount);
};

#define USE_UI_CLASS DemoProgram